package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class MyApp2Application {

	public static void main(String[] args) {
		SpringApplication.run(MyApp2Application.class, args);
	}
	
	@RequestMapping("/")
	public String hello() {
		return "Hello, Spring Boot2";
	}
	
}
